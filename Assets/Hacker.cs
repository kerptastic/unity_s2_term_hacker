﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hacker : MonoBehaviour
{
    private struct Level
    {
        string m_name;
        public string Name
        {
            get { return m_name; }
        }

        string[] m_validPasswords;
        public string[] ValidPasswords
        {
            get { return m_validPasswords; }
        }

        string m_currentPassword;
        public string CurrentPassword
        {
            get { return m_currentPassword; }
        }

        string m_reward;
        public string Reward
        {
            get { return m_reward; }
        }

        public Level(string name, string[] validPasswords, string reward)
        {
            m_name = name;
            m_validPasswords = validPasswords;
            m_currentPassword = m_validPasswords[0];
            m_reward = reward;
        }

        public void RandomizePassword()
        {
            var rdm = UnityEngine.Random.Range(0, m_validPasswords.Length);
            m_currentPassword = m_validPasswords[rdm];

            Debug.Log(m_currentPassword);
        }
    }

    private readonly List<Level> m_levels = new List<Level>
    {
        new Level("White House", new string[] { "noBush", "crookedHillary", "trumpSucks"}, "White House Reward"),
        new Level("FBI", new string[] { "FBIAccessPlease", "comeyIsGod"}, "FBI Reward"),
        new Level("NASA", new string[] { "BuzzLightyear", "HOUSTONwehaveaproblem", "moonLANDINGisFAKE"}, "NASA Reward")
    };

    private enum Screen
    {
        MainMenu, PasswordGuess, PlayerWin
    };

    private Level m_currentLevel;
    private Screen m_currentScreen;

    // Start is called before the first frame update
    void Start()
    {
        ShowMainMenu();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnUserInput(string input)
    {
        try
        {
            if (input == "menu")
            {
                ShowMainMenu();
                return;
            }

            switch (m_currentScreen)
            {
                case Screen.MainMenu:
                    SelectLevel(input);
                    break;
                case Screen.PasswordGuess:
                    CheckPassword(input);
                    break;
                default:
                    break;
            };
        }
        catch (FormatException)
        {
            Terminal.WriteLine("Invalid option: " + input);
        }
    }

    void ShowMainMenu()
    {
        m_currentScreen = Screen.MainMenu;

        Terminal.ClearScreen();
        Terminal.WriteLine("Hacking Console");

        var index = 1;

        foreach (Level level in m_levels)
        {
            Terminal.WriteLine(index + ". " + level.Name);
            index++;
        }

        Terminal.WriteLine("\nSelect agency to hack into: ");
    }

    void SelectLevel(string input)
    {
        var choice = int.Parse(input);
        var levelIndex = choice - 1;

        if (choice > 0 && choice <= m_levels.Count)
        {
            m_currentLevel = m_levels[levelIndex];
            StartGame();
        }
        else
        {
            Terminal.WriteLine("Invalid option: " + input);
        }
    }

    void StartGame()
    {
        m_currentScreen = Screen.PasswordGuess;

        m_currentLevel.RandomizePassword();

        Terminal.ClearScreen();
        Terminal.WriteLine("Enter " + m_currentLevel.Name + " Password: ");
    }

    void CheckPassword(string input)
    {
        m_currentScreen = Screen.PasswordGuess;

        if (input == m_currentLevel.CurrentPassword)
        {
            Terminal.WriteLine("Access Granted! Welcome!");
            ShowPlayerWin();
        }
        else
        {
            Terminal.WriteLine("\nAccess DENIED.\nHere's a hint: " + m_currentLevel.CurrentPassword.Anagram());
        }
    }

    void ShowPlayerWin()
    {
        m_currentScreen = Screen.PlayerWin;

        Terminal.ClearScreen();
        Terminal.WriteLine("Welcome to the " + m_currentLevel.Name + " Database.");
        Terminal.WriteLine("");

        Terminal.WriteLine(m_currentLevel.Reward);

        Terminal.WriteLine("");
        Terminal.WriteLine("Type \"menu\" to return to the Hacker Console.");
    }
}

